package com.whitebirch.demo.controller;

import com.whitebirch.demo.pojo.Employee;
import com.whitebirch.demo.pojo.Training;
import com.whitebirch.demo.pojo.TrainingType;
import com.whitebirch.demo.service.EmployeeEnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author AmbikaBV
 * Main Rest controller for employee services
 */
@RestController
public class EmployeeController {

    @Autowired
    EmployeeEnrollmentService service;

    @RequestMapping(value = "/trainings", method = RequestMethod.GET)
    public List<String> getTrainings() {

        List<String> trainings = new ArrayList<String>();
        Arrays.stream(TrainingType.values()).forEach(training -> trainings.add(training.toString()));
        return trainings;
    }

    @GetMapping(value = "/employee/trainings")
    public List<Training> getEmployeeTrainings(@PathVariable String employeeID) {

        return service.getEmployeeTrainings(employeeID);
    }

    @PostMapping(value = "/employee/addEmployee")
    public ResponseEntity<Void> addEmployee(@RequestBody Employee employee) {

        Employee emp = service.addEmployee(employee);
        if (emp == null)
            return ResponseEntity.noContent().build();
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(emp.getEmpId()).toUri();
        return ResponseEntity.created(location).build();

    }

    @GetMapping(value = "/employee/{empID}")
    public Employee getEmployee(@PathVariable String empID) {

        return service.getEmployee(empID);

    }

    @RequestMapping(value = "/employee/{employeeID}/training/", method = RequestMethod.POST)
    public ResponseEntity<Void> enrollTraining(@PathVariable String employeeID, @RequestBody Training training) {
        Training tr = service.enrollTraining(employeeID, training);
        if (tr == null)
            return ResponseEntity.noContent().build();
        return new ResponseEntity<Void>(HttpStatus.OK);

    }

}

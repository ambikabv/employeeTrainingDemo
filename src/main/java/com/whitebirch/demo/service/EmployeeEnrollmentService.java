package com.whitebirch.demo.service;

import com.whitebirch.demo.pojo.Employee;
import com.whitebirch.demo.pojo.Training;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class EmployeeEnrollmentService {

    private static List<Employee> employeeList = new ArrayList<Employee>();
    private SecureRandom random = new SecureRandom();

    static {

        Training tr1 = new Training("1", "Spring", "Spring Training");
        Training tr2 = new Training("2", "SOAP", "SOAP Training");
        Training tr3 = new Training("3", "Rest", "Rest Training");
        Employee emp1 = new Employee("1001", "Goerge", "Clooney", 59, new ArrayList<Training>(Arrays
                .asList(tr1, tr2)));
        Employee emp2 = new Employee("1002", "Sheldon", "Cooper", 32, new ArrayList<Training>(Arrays
                .asList(tr3, tr2)));

    }

    public List<Training> getEmployeeTrainings(String employeeID) {
        List<Training> trList = new ArrayList<Training>();
        employeeList.stream()
                .filter(employee -> employee.getEmpId() == employeeID)
                .forEach(employee -> trList.addAll(employee.getTraining()));
        return trList;

    }

    public Employee addEmployee(Employee employee) {
        if (getEmployee(employee.getEmpId()) == null) {
            String randomId = new BigInteger(130, random).toString(32);
            employee.setEmpId(randomId);
            employeeList.add(employee);
        }
        return null;
    }

    public Employee getEmployee(String empID) {
        for (Employee exst : employeeList) {
            if (exst.getEmpId() != null && exst.getEmpId().equals(empID)) {
                return exst;
            }
        }
        return null;
    }

    public Training enrollTraining(String employeeID, Training training) {
        Employee employee = getEmployee(employeeID);
        if (employee == null)
            return null;
        List<Training> trainingList = employee.getTraining();
        if (trainingList == null) {
            trainingList = new ArrayList<Training>();
            employee.setTraining(trainingList);
        }
        trainingList.add(training);

        return training;

    }
}

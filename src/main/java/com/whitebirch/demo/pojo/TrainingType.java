package com.whitebirch.demo.pojo;

/**
 * @author AmbikaBV
 * <p>
 * TrainingType model - holds static data for this demo
 */
public enum TrainingType {
    SPRING("Spring"),
    HIBERNATE("Hibernate"),
    REST("Rest"),
    SOAP("SOAP"),
    J2EE("J2ee"),
    SPRING_SECURITY("Spring Security");

    private final String trainingtype;

    private TrainingType(String training) {
        trainingtype = training;

    }

}

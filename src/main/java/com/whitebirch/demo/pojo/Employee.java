package com.whitebirch.demo.pojo;

import java.util.List;


/**
 * @author AmbikaBV
 * <p>
 * Employee model
 */
public class Employee {

    private String empId;

    private String firstName;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Training> getTraining() {
        return training;
    }

    public void setTraining(List<Training> training) {
        this.training = training;
    }

    private String lastName;

    private int age;

    private List<Training> training;

    public Employee(String empId, String firstName, String lastName, int age, List<Training> training) {
        this.empId = empId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.training = training;

    }


}

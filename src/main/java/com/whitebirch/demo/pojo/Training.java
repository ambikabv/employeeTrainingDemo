package com.whitebirch.demo.pojo;

/**
 * @author AmbikaBV
 * <p>
 * Training model
 */
public class Training {

    private String trainingId;
    private String training;
    private String desc;

    public String getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(String trainingId) {
        this.trainingId = trainingId;
    }

    public String getTraining() {
        return training;
    }

    public void setTraining(String training) {
        this.training = training;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Training(String trainingId, String training, String desc) {
        this.trainingId = trainingId;
        this.training = training;
        this.desc = desc;
    }
}

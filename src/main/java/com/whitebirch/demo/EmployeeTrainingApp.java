package com.whitebirch.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author AmbikaBV
 * <p>
 * Application entry point
 */
@SpringBootApplication
public class EmployeeTrainingApp {
    public static void main(String[] args) {
        SpringApplication.run(EmployeeTrainingApp.class, args);
    }
}
